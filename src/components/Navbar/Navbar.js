import React from 'react'
import { Link } from 'react-router-dom'
import { StyleSheet, css } from 'aphrodite';

export default function Navbar() {
    return (
        <nav className={css(styles.nav)}>
            <Link to="/">Accueil</Link>
            <Link to="/services">Services</Link>
        </nav>
    )
}
const styles = StyleSheet.create({
    nav: {
        backgroundColor: "blue",
        display: "flex",
        justifyContent: "space-around",
        textColor:"black",
    },
});