import React from 'react';
import { Link ,Outlet} from 'react-router-dom';

export default function Services() {
  return (
    <div>
        <h1>Services</h1>
        <nav>
            <Link to="/services/marketing">Marketing</Link>
            <Link to="/services/developpement">developpement</Link>
        </nav>
        <Outlet/>
    </div>
  )
}
