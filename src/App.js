import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Home from './components/Home/Home';
import Services from './components/Services/Services';
import Profile from './components/Profile/Profile';
import Navbar from './components/Navbar/Navbar';
import Marketing from './components/Services/Marketing/Marketing';
import Developement from './components/Services/Developement/Developement';
const App = () => {
  return (
    <div>
      <Navbar/>
      <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/services" element={<Services />}>
        <Route path="/services/marketing" element={<Marketing />}/>
        <Route path="/services/developpement" element={<Developement />}/>
        <Route/>
         </Route>
      <Route path="/profil/:id" element={<Profile />} />
      </Routes>
    </div>
  );
};

export default App;